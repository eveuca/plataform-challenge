variable "aws_region" {
  type    = string
  default = "sa-east-1"
}

variable "ecr_bucket_name" {
  type    = string
  default = "iti-bucket-arakaki-in"
}

variable "vpc_cidr_block" {
  type    = string
  default = "10.0.0.0/16"
}

variable "subnet_cidr_block" {
  type    = string
  default = "10.0.1.0/24"
}

variable "public_key_path" {
  type        = string
  description = <<DESCRIPTION
Path to the SSH public key to be used for authentication.
Ensure this keypair is added to your local SSH agent so provisioners can
connect.
Example: ~/.ssh/terraform.pub
DESCRIPTION
}
