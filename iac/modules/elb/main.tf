resource "aws_security_group" "elb" {
  name        = var.scg_name
  description = var.scg_description
  vpc_id      = var.vpc_id

# HTTP access from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

# outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_elb" "web" {
  name = var.elb_name

  subnets         = [var.subnet_id]
  security_groups = [aws_security_group.elb.id]
  instances       = [var.instance_id]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
}
