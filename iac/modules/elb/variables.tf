variable "scg_name" {
  type = string
}

variable "scg_description" {
  type    = string
  default = "ELB security group with HTTP access on port 80"
}

variable "elb_name" {
  type = string
}

variable "subnet_id" {
  type = string
}

variable "instance_id" {
  type = string
}

variable "vpc_id" {
  type = string
}
