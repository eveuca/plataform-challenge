resource "aws_vpc" "default" {
  cidr_block = var.vpc_cidr_block
}

resource "aws_internet_gateway" "default" {                                       
  vpc_id = aws_vpc.default.id
}                                                                                 

resource "aws_route" "internet_access" {                                          
  route_table_id         = aws_vpc.default.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.default.id
}

resource "aws_subnet" "default" {                                                 
  vpc_id                  = aws_vpc.default.id
  cidr_block              = var.subnet_cidr_block
  map_public_ip_on_launch = true
}

resource "aws_security_group" "default" {
  name        = var.vpc_scg_name
  description = var.vpc_scg_description
  vpc_id      = aws_vpc.default.id

# SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

# HTTP access from the VPC
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

# outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


