variable "vpc_cidr_block" {
  type = string
}

variable "subnet_cidr_block" {
  type = string
}

variable "vpc_scg_name" {
  type    = string
  default = "scg-group-auto"
}

variable "vpc_scg_description" {
  type    = string
  default = "allow ssh and http from internet" 
}
