variable "aws_region" {
  type = string
}

variable "ec2_user" {
  type = string
}

variable "ec2_ami" {
  type = string
}

variable "ec2_ami_owner" {
  type = string
}

variable "ec2_flavour" {
  type = string
}

variable "subnet_id" {
  type = string 
}

variable "scg_id" {
  type = string
}

variable "key_id" {
  type = string
}
