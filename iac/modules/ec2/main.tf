data "aws_ami" "os" {
  most_recent = true

  filter {
    name   = "name"
    values = [var.ec2_ami]
  }

  owners = [var.ec2_ami_owner]
}



resource "aws_instance" "k8s" {
  connection {
    user = var.ec2_user
    host = "${self.public_ip}"
  }

  instance_type = var.ec2_flavour
  ami = data.aws_ami.os.id
  key_name = var.key_id
  vpc_security_group_ids = [var.scg_id]
  subnet_id = var.subnet_id

}
