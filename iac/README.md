# IaC teraform files

This folder groups all terraform files, mostly in module strucutured form. As described in HashiCorp documentation, this variation of versioning terraform files can extend with a environment folder with multiple definitions of terraform files per environment (dev, stg, prd).

### TODO
 - get away with this private key
 - improve ecs module definition
 - implement ASG module definition
 - use ansible as configuration managet(?)


### Feedback

A ideia do desafio de vocês é muito boa, fiz com muito prazer, e também aprendi umas artimanhas de gitlab novas, como vocês podem ver pela estrutura de gatilhos. Peço as mais sinceras desculpas pela demora ao entregar este documento. Vocês vão observar pelos horários dos commits que quando eu peguei pra fazer, fiz o máximo que consegui. Tenho muito para criticar desse meu trabalho também, mas pela minha disponibilidade atual, tá valendo!
