provider "aws" {
  region = var.aws_region
}

terraform {
  backend "s3" {
    bucket = "terraform-backend-arakaki-in-company"
    key    = "workspaces-example/terraform.tfstate"
    region = "sa-east-1"
  }
}

module "ecr" {
  source = "./modules/ecr"
  name   = "iti-bucket-arakaki-in"
}

module "vpc" {
  source            = "./modules/vpc"
  vpc_cidr_block    = var.vpc_cidr_block
  subnet_cidr_block = var.subnet_cidr_block
}

resource "aws_key_pair" "auth" {
  key_name   = "iti-pem-arakaki-in"
  public_key = "${file(var.public_key_path)}"
}


# a ideia desse modulo era instalar um Kind (https://github.com/kubernetes-sigs/kind) nessa 
# maquina virtual, mas não compactuo com o uso de remote exec para configuração de maquinas 
# virtuais. O adequado é rodar ansible para fazer a configuração da maquina. Nesse contexto,
#entrego esse repositorio modularizado, mas essa vm não vai ser o ambiente de deploy. 

module "ec2" {
  source      = "./modules/ec2"
  ec2_user    = "centos"
  ec2_flavour = "t2.micro"
  ec2_ami     = "CentOS Linux 7 x86_64 HVM EBS 1708_11.01-b7ee8a69-ee97-4a49-9e68-afaee216db2e-ami-95096eef.4"
  ec2_ami_owner = "679593333241"
  subnet_id   = module.vpc.subnet_id
  scg_id      = module.vpc.scg_id
  key_id      = aws_key_pair.auth.id
  aws_region  = var.aws_region 
}

module "elb" {
  source      = "./modules/elb"
  elb_name    = "iti-elb-arakaki-in"
  scg_name    = "iti-scg-elb-arakaki-in"
  vpc_id      = module.vpc.vpc_id
  subnet_id   = module.vpc.subnet_id
  instance_id = module.ec2.ec2_id
}
