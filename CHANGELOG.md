# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.3](https://gitlab.com/eveuca/plataform-challenge/compare/v0.0.2...v0.0.3) (2020-03-31)


### Features

* **ITI1-007:** adding docker build and push to app pipeline ([9728e65](https://gitlab.com/eveuca/plataform-challenge/commit/9728e65e464b55bd62200de75e03c401cebdbdf1))
* **ITI1-008:** add readme.md and comments on the pipeline next steps ([1279088](https://gitlab.com/eveuca/plataform-challenge/commit/12790880e32a205b129f32562313d60ab1907600))
* **ITI1-008:** testing helm template ([8e6a850](https://gitlab.com/eveuca/plataform-challenge/commit/8e6a8508f28daf522bb1b2549aa36a171d560a25))
* **ITI2-007:** adding README.md documentation ([de24c9e](https://gitlab.com/eveuca/plataform-challenge/commit/de24c9ea6bc984664208a7f602689f4ead539e7c))
* **ITI2-008:** add ec2 configuration script ([5606b20](https://gitlab.com/eveuca/plataform-challenge/commit/5606b2036fb6fc42e91401383f8d4dc0c2162fbc))
* **ITI2-008:** add ec2 configuration script ([16d9208](https://gitlab.com/eveuca/plataform-challenge/commit/16d9208dab736d9ac51ccde0839f005955212d56))


### Bug Fixes

* **ITI1-007:** change image context ([ec6eef6](https://gitlab.com/eveuca/plataform-challenge/commit/ec6eef6da3e113fb206784e36d83374bb5d297c2))

### [0.0.2](https://gitlab.com/eveuca/plataform-challenge/compare/v0.0.1...v0.0.2) (2020-03-29)


### Features

* **ITI1-006:** new repo structure ([92fc4ab](https://gitlab.com/eveuca/plataform-challenge/commit/92fc4abd1a3a4ebf01eb39542211b49422701aac))
* **ITI1-006:** using aws cli version 2.0 ([0ccf389](https://gitlab.com/eveuca/plataform-challenge/commit/0ccf389d25ebd93d06d81bde8a42f19649dccc5b))
* **ITI2-001:** initial terraform files for docker container registry ([9aba517](https://gitlab.com/eveuca/plataform-challenge/commit/9aba51786f83340d7f1fecdc0ee0cc6521349ab5))
* **ITI2-001:** initial terraform pipeline file ([1ca6cda](https://gitlab.com/eveuca/plataform-challenge/commit/1ca6cda43bf3e9ab606f69ccd8360ede2385e2da))
* **ITI2-002:** provision vpc resources ([291603d](https://gitlab.com/eveuca/plataform-challenge/commit/291603d8bdcc25328222e02025b772d6a8e27182))
* **ITI2-003:** add elastic load balancer modules and adjust vpc ([5b5e91b](https://gitlab.com/eveuca/plataform-challenge/commit/5b5e91ba3f2d7f541ccb25646df27044f2ef6cd9))
* **ITI2-004:** adding EC2 keypair ([3a2c521](https://gitlab.com/eveuca/plataform-challenge/commit/3a2c521b914f4c614bc65c2b99f37d991327f35a))
* **ITI2-005:** deploying ec2 with kind ([8e7c81b](https://gitlab.com/eveuca/plataform-challenge/commit/8e7c81bf0606f28e4bcedbf4847d2195196a6513))
* **ITI2-005:** hora de ir dormir ([9e5278c](https://gitlab.com/eveuca/plataform-challenge/commit/9e5278cbdfa556128454a8bfcd67e227df93a3f6))
* **ITI2-006:** adjust pipeline last details ([f23a0c8](https://gitlab.com/eveuca/plataform-challenge/commit/f23a0c8fd0ca65b797ce7b9a96840161762d66d6))
* **ITI2-006:** adjusting .gitignore ([035cc40](https://gitlab.com/eveuca/plataform-challenge/commit/035cc404f44ac6704408a0136bd66830cbafc397))


### Bug Fixes

* **ITI1-005:** remove package-lock.json from repo ([8e42022](https://gitlab.com/eveuca/plataform-challenge/commit/8e42022ccc5ba84def3da08b926548c3ec69c878))
* **ITI1-006:** adapt ci files after lint ([5b5e355](https://gitlab.com/eveuca/plataform-challenge/commit/5b5e355a90f68ce861106ba925e1a676782f5ea5))
* **ITI1-006:** adapt file name ([d890525](https://gitlab.com/eveuca/plataform-challenge/commit/d890525514aee2b4b9ddec7da1f85ccd6e1a2e00))
* **ITI1-006:** adapt file names ([ef76c94](https://gitlab.com/eveuca/plataform-challenge/commit/ef76c9474b7a5bb86e31ff012abeda2855627414))
* **ITI1-006:** adapt helm values ([8e51ee7](https://gitlab.com/eveuca/plataform-challenge/commit/8e51ee7a5ad6c0198b986dc8d145aef95b647575))
* **ITI1-006:** remove typo from pipeline file ([c1b9c9a](https://gitlab.com/eveuca/plataform-challenge/commit/c1b9c9a9659a617c605dcb96f876d2f7011f88b5))
* **ITI2-001:** use new image for terraform apply commands ([d882d81](https://gitlab.com/eveuca/plataform-challenge/commit/d882d81e8bd9800ae2dedc92731b7d71e9685413))
* **ITI2-002:** adjust typo in gitlab-ci.ymlfile ([6d91fdf](https://gitlab.com/eveuca/plataform-challenge/commit/6d91fdfe7b6668505e77b42a4cb22c5a8048fa5d))
* **ITI2-002:** fix bucket backend region to use hardcoded variable ([e5ae52e](https://gitlab.com/eveuca/plataform-challenge/commit/e5ae52e4bcf2d92b926ce44dedcaaaae8b8adeab))
* **ITI2-003:** adjust variable file ([90d9725](https://gitlab.com/eveuca/plataform-challenge/commit/90d97256ad7b7d1df17629212a42d2ee083aa9f5))
* **ITI2-005:** adapt ami injection ([1ab7dc3](https://gitlab.com/eveuca/plataform-challenge/commit/1ab7dc3792c1cbe1d37ee1ec96ff7df65afd83b3))
* **ITI2-005:** added new output parameter to vpc ([3105021](https://gitlab.com/eveuca/plataform-challenge/commit/3105021c3fb2f0713b5ccc85010030b42bef836e))
* **ITI2-005:** added region to ec2 ([3816641](https://gitlab.com/eveuca/plataform-challenge/commit/38166410c0600c7b931f5d67c312621e6e60c47d))
* **ITI2-005:** fix typo in ami definition ([6ddcdde](https://gitlab.com/eveuca/plataform-challenge/commit/6ddcddea24cee31ae1a0f1ff4ccae055a1bf4072))
* **ITI2-005:** fix typo in ami definition ([7d1e542](https://gitlab.com/eveuca/plataform-challenge/commit/7d1e5421594e69f894165d2f232ed2eae0e6a464))
* **ITI2-005:** fix typo in ami owner definition ([c93c95c](https://gitlab.com/eveuca/plataform-challenge/commit/c93c95cf65450245592615cc12a3c9c992a12dea))
* **ITI2-005:** fix typo in ec2 flavour ([7c8e4d0](https://gitlab.com/eveuca/plataform-challenge/commit/7c8e4d08e0a4b530473b897ac19edc6e7077c2cf))
* **ITI2-005:** fix typo in ec2 user ([75769e0](https://gitlab.com/eveuca/plataform-challenge/commit/75769e0f96f430223ab22f7489e58e439f3916db))
* **ITI2-005:** try new ami search ([23fddaf](https://gitlab.com/eveuca/plataform-challenge/commit/23fddafaaa04bc3b6e3c0a12445368960beff8b6))
* **ITI2-005:** typo in init script ([af2e812](https://gitlab.com/eveuca/plataform-challenge/commit/af2e8124ab99c4a8a5ca02e2493a7fcfc0b3dbaa))
* **ITI2-006:** added missing output variable ([86c05be](https://gitlab.com/eveuca/plataform-challenge/commit/86c05bea62eafbbcdaf5033883643a62d6a102ff))
* **ITI2-006:** typo in elb module source ([74a40b5](https://gitlab.com/eveuca/plataform-challenge/commit/74a40b526157ccb396d6862f81338a5e2db498ed))

### 0.0.1 (2020-03-08)


### Features

* **ITI1-001:** add package.json to gitignore ([1ca79d4](https://gitlab.com/eveuca/plataform-challenge/commit/1ca79d49697b97f8fbc6d320a1046e932895be67))
* **ITI1-001:** add cid folder structure and helm basic files ([5481e1a](https://gitlab.com/eveuca/plataform-challenge/commit/5481e1a58c4c12ef217af71a5a98a50bb975e6ba))
* **ITI1-002:** adapt helm template files ([ba40905](https://gitlab.com/eveuca/plataform-challenge/commit/ba4090568f819acccdb77335d07ed937a79ce2d0))
* **ITI1-003:** add initial gitlab CI child/parent pipeline files ([a031b5a](https://gitlab.com/eveuca/plataform-challenge/commit/a031b5affabb3d76a52b8931d73e20ffb83c3274))
* **ITI1-003:** change files name and location to segregate responsabilitis ([9824c49](https://gitlab.com/eveuca/plataform-challenge/commit/9824c4940900f207e754814f7ba209b8e7ca796c))
* **ITI1-004:** add Dockerfile ([565f6bf](https://gitlab.com/eveuca/plataform-challenge/commit/565f6bf1ab23632739b137d9f3cc1fd1acc6618f))
* **ITI1-005:** add gitlab ci file to build the app container ([3685241](https://gitlab.com/eveuca/plataform-challenge/commit/3685241691740eb1ad7ae36b1b7dc341275e9424))


### Bug Fixes

* **ITI1-002:** remove imagePullSecrets due to using public container registry ([3b4f80d](https://gitlab.com/eveuca/plataform-challenge/commit/3b4f80d0aef0a3f22d0773cacecb3a2c104d219c))
